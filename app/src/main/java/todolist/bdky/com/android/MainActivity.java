package todolist.bdky.com.android;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import todolist.bdky.com.android.app.AppConfig;
import todolist.bdky.com.android.app.AppController;
import todolist.bdky.com.android.helper.SessionManager;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = LoginActivity.class.getSimpleName();
    ListView listView;
    ArrayList<String> arrayList;
    ArrayAdapter<String> arrayAdapter;
    String messageText;
    int position;
    public JSONArray personalNotes;

    //NotificationCompat.Builder notification;
    //private static final int uniqueId = 45612;

    NotificationManager mNotificationManager;
    Notification notification;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        makeGETRequestForPersonalNotes(Integer.toString(SessionManager.currentUserID));


       // notification = new NotificationCompat.Builder(this);
       // notification.setAutoCancel(true);


        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
// The id of the channel.
        String id = "my_channel_01";
// The user-visible name of the channel.
        CharSequence name = getString(R.string.channel_name);
// The user-visible description of the channel.
        String description = getString(R.string.channel_description);
        int importance = NotificationManager.IMPORTANCE_LOW;
        NotificationChannel mChannel = new NotificationChannel(id, name, importance);
// Configure the notification channel.
        mChannel.setDescription(description);
        mChannel.enableLights(true);
// Sets the notification light color for notifications posted to this
// channel, if the device supports this feature.
        mChannel.setLightColor(Color.RED);
        mChannel.enableVibration(true);
        mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});

        mNotificationManager.createNotificationChannel(mChannel);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        listView = (ListView) findViewById(R.id.listView);
        arrayList = new ArrayList<>();
        arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arrayList);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent();
                intent.setClass(MainActivity.this, EditMessageClass.class);
                intent.putExtra(Intent_Constant.INTENT_MESSAGE_DATA, arrayList.get(i).toString());
                intent.putExtra(Intent_Constant.INTENT_ITEM_POSITION, i);
                startActivityForResult(intent, Intent_Constant.INTENT_REQUEST_CODE_TWO);

            }
        });


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                        */
                Intent intent = new Intent();
                intent.setClass(MainActivity.this, EditFieldClass.class);
                startActivityForResult(intent, Intent_Constant.INTENT_REQUEST_CODE);
            }

        });


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        setupListViewListener();
    }

    private void setupListViewListener() {
        listView.setOnItemLongClickListener(
                new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> adapter,
                                                   View item, int pos, long id) {

                            createDialog(pos);

                        // arrayList.remove(pos);
                        // Refresh the adapter
                        //arrayAdapter.notifyDataSetChanged();
                        // Return true consumes the long click event (marks it handled)
                        return true;
                    }
                });
    }

    private void createDialog(final int pos) {


        AlertDialog.Builder alertDlg = new AlertDialog.Builder(this);
        alertDlg.setMessage("Silmek istediğinizden emin misiniz ?");
        alertDlg.setCancelable(false);
        alertDlg.setPositiveButton("Evet", new DialogInterface.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                    sendNotification();
                    arrayList.remove(pos);
                    arrayAdapter.notifyDataSetChanged();


            }
        });

        alertDlg.setNegativeButton("Hayir", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        alertDlg.create().show();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void sendNotification() {


/*
        notification.setSmallIcon(R.mipmap.ic_launcher); // test amacli yapilmistir.
        notification.setTicker("Test Ticker");
        notification.setWhen(System.currentTimeMillis());
        notification.setContentTitle("Listeden bir item silindi !");
        notification.setContentText("Uygulamaya donup bir kontrol etmek ister misiniz ?");
        //String channelId = "bdky";
        notification.setChannel(channelId);
        Intent intent2 = new Intent(this, MainActivity.class);
        PendingIntent pi = PendingIntent.getActivity(this, 0 , intent2, PendingIntent.FLAG_UPDATE_CURRENT);

        notification.setContentIntent(pi);

        //handle notification
        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nm.notify(uniqueId,notification.build());
*/



        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        // Sets an ID for the notification, so it can be updated.
        int notifyID = 1;
        // The id of the channel.
        String CHANNEL_ID = "my_channel_01";
        // Create a notification and set the notification channel.
        notification = new Notification.Builder(MainActivity.this)
                .setContentTitle("Oge silindi")
                .setContentText("TodoList uygulamasinda bir oge silindi. Kontrol etmek icin tiklayin !")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setChannelId(CHANNEL_ID)
                .build();

        // Issue the notification.
        mNotificationManager.notify(notifyID, notification);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Intent_Constant.INTENT_REQUEST_CODE) { // resultcode olabilir mi ?
//            messageText = data.getStringExtra(Intent_Constant.INTENT_MESSAGE_FIELD);
//            arrayList.add(messageText);
//            arrayAdapter.notifyDataSetChanged();
              makeGETRequestForPersonalNotes(Integer.toString(SessionManager.currentUserID));
        } else if (resultCode == Intent_Constant.INTENT_REQUEST_CODE_TWO) {
            messageText = data.getStringExtra(Intent_Constant.INTENT_CHANGED_MESSAGE);
            position = data.getIntExtra(Intent_Constant.INTENT_ITEM_POSITION, -1);
            arrayList.remove(position);
            arrayList.add(position, messageText);
            arrayAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_kisisel_notlar) {
            Intent intent = new Intent(this, DisplayPersonalNotesActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_grup_notlari) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void makeGETRequestForPersonalNotes(final String user_id) { //kullanıcıya ait tüm personal notları "personalNotes" isimli jsonArray'ine koyar
        String params[] = {"user_id"};
        String values[] = {user_id};
        String url = AppController.addLocationToUrl(AppConfig.URL_CREATE_PERSONAL_NOTE, params, values);
        Log.d(TAG, "url: " + url);

        StringRequest strReq = new StringRequest(Request.Method.GET,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Personal notes get Response: " + response.toString());

                try {

                    personalNotes = new JSONArray(response);
                    arrayList.clear();

                    for (int i = 0; i < personalNotes.length(); i++) {

                        JSONObject note = (JSONObject) personalNotes.get(i);

                        int noteID = note.getInt("id");
                        String title = note.getString("title");
                        String description = note.getString("description");
                        arrayList.add(title); //notların başlıklarını ekrandaki listeye ekler. Pek mantıklı değil çünkü ekranda başlığı gözüken o notun hangi id'ye sahip olduğunu bilemiyoruz

                    }
                    arrayAdapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        "Hata", Toast.LENGTH_LONG).show();
            }
        });
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);
    }

    private void makeDELETERequestForAPersonalNote(String noteID) { //bozuk
        String params[] = {"user_id", "id"};
        String values[] = {Integer.toString(SessionManager.currentUserID), noteID};
        String url = AppController.addLocationToUrl(AppConfig.URL_CREATE_PERSONAL_NOTE, params, values);
        Log.d(TAG, "url: " + url);

        StringRequest strReq = new StringRequest(Request.Method.DELETE,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Personal note delete Response: " + response.toString());

                try {

                    personalNotes = new JSONArray(response);

                    for (int i = 0; i < personalNotes.length(); i++) {

                        JSONObject note = (JSONObject) personalNotes.get(i);

                        int noteID = note.getInt("id");
                        String title = note.getString("title");
                        String description = note.getString("description");
                        arrayList.add(title);
                    }

                    arrayAdapter.notifyDataSetChanged();

                    Toast.makeText(getApplicationContext(), "Başarıyla silindi", Toast.LENGTH_LONG).show();

                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        "Hata", Toast.LENGTH_LONG).show();
            }
        });
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);
    }
}
