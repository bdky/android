package todolist.bdky.com.android.app;

public class AppConfig {
    // Server user login url
    public static String URL_LOGIN = "https://bdky-web-server.herokuapp.com/users/sign_in.json";

    // Server user register url
    public static String URL_REGISTER = "https://bdky-web-server.herokuapp.com/users.json";

    public static String URL_CREATE_PERSONAL_NOTE = "https://bdky-web-server.herokuapp.com/android.json";
}