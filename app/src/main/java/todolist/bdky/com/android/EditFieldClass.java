package todolist.bdky.com.android;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import todolist.bdky.com.android.app.AppConfig;
import todolist.bdky.com.android.app.AppController;
import todolist.bdky.com.android.helper.SessionManager;


public class EditFieldClass extends AppCompatActivity {
    private static final String TAG = LoginActivity.class.getSimpleName();
    NotificationManager mNotificationManager;
    Notification notification;
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
// The id of the channel.
        String id = "my_channel_01";
// The user-visible name of the channel.
        CharSequence name = getString(R.string.channel_name);
// The user-visible description of the channel.
        String description = getString(R.string.channel_description);
        int importance = NotificationManager.IMPORTANCE_LOW;
        NotificationChannel mChannel = new NotificationChannel(id, name, importance);
// Configure the notification channel.
        mChannel.setDescription(description);
        mChannel.enableLights(true);
// Sets the notification light color for notifications posted to this
// channel, if the device supports this feature.
        mChannel.setLightColor(Color.RED);
        mChannel.enableVibration(true);
        mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});

        mNotificationManager.createNotificationChannel(mChannel);

        setContentView(R.layout.to_do_layout);

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void saveButtonClicked(View v) {
        String titleText = ((EditText) findViewById(R.id.et_title)).getText().toString();
        String messageText = ((EditText) findViewById(R.id.message)).getText().toString();
        if (messageText.equals("") && titleText.equals("")) {
            //do nothing
        } else {
            sendPersonalNoteToServer(titleText, messageText);
            sendAddedNotification();
            Intent intent = new Intent();
            intent.putExtra(Intent_Constant.INTENT_MESSAGE_FIELD, messageText);
            setResult(Intent_Constant.INTENT_RESULT_CODE, intent);


            finish();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void sendAddedNotification() {

            mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            // Sets an ID for the notification, so it can be updated.
            int notifyID = 1;
            // The id of the channel.
            String CHANNEL_ID = "my_channel_01";
            // Create a notification and set the notification channel.
            notification = new Notification.Builder(EditFieldClass.this)
                    .setContentTitle("Oge eklendi")
                    .setContentText("TodoList uygulamasinda bir oge eklendi. Kontrol etmek icin tiklayin !")
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setChannelId(CHANNEL_ID)
                    .build();

            // Issue the notification.
            mNotificationManager.notify(notifyID, notification);


    }

    private void sendPersonalNoteToServer(final String title, final String description) {

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_CREATE_PERSONAL_NOTE, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Personal note send Response: " + response.toString());

                try {

                    JSONObject jObj = new JSONObject(response);

                    Toast.makeText(getApplicationContext(), "Başarıyla eklendi", Toast.LENGTH_LONG).show();

                    finish();

                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        "Hata", Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting parameters
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", Integer.toString(SessionManager.currentUserID));
                params.put("title", title);
                params.put("description", description);


                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);
    }
}
